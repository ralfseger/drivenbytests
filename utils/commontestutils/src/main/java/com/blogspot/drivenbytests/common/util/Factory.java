package com.blogspot.drivenbytests.common.util;

public interface Factory<T> {
	T createInstance();
}
