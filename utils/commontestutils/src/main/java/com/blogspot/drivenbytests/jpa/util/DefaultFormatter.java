package com.blogspot.drivenbytests.jpa.util;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

public class DefaultFormatter {

	protected SimpleDateFormat DATUM = new SimpleDateFormat("yyyy.MM.dd");

	private static final String PROPERTY_SEPARATOR = ", ";

	private final Map<Class<?>, Map<Object, Integer>> typeInstanceMap = new HashMap<Class<?>, Map<Object, Integer>>();

	public static void appendProperty(final StringBuilder builder,
			final String propertyName, final Object propertyValue) {
		appendProperty(builder, PROPERTY_SEPARATOR, propertyName, propertyValue);
	}

	public static void appendProperty(final StringBuilder builder,
			final String prefix, final String propertyName,
			final Object propertyValue) {
		if (propertyValue instanceof Number) {
			builder.append(String.format("%s%s:%s", prefix, propertyName,
					propertyValue));
		} else {
			builder.append(String.format("%s%s:'%s'", prefix, propertyName,
					propertyValue));
		}
	}

	public static String endString(final StringBuilder builder) {
		return builder.append("}").toString();
	}

	public StringBuilder startStringBuilder(Object object) {
		final StringBuilder start = new StringBuilder(object.getClass()
				.getSimpleName()).append("{");
		Object id = ermittleId(object);
		appendProperty(start, "", "id", id);
		return start;
	}

	// helper

	private Integer ermittleId(Object object) {
		final Map<Object, Integer> instanceMap;
		Class<? extends Object> typeKey = object.getClass();
		if (typeInstanceMap.containsKey(typeKey)) {
			instanceMap = typeInstanceMap.get(typeKey);
		} else {
			instanceMap = new HashMap<Object, Integer>();
			typeInstanceMap.put(typeKey, instanceMap);
		}
		Integer id = instanceMap.get(object);
		if (id == null) {
			id = 1 + instanceMap.size();
			instanceMap.put(object, id);
		}
		return id;
	}

}
