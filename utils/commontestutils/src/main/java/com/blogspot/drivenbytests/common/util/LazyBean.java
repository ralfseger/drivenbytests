package com.blogspot.drivenbytests.common.util;

public class LazyBean<T> {
	private final Factory<T> factory;
	private T instance;

	public LazyBean(final Factory<T> factory) {
		this.factory = factory;
	}

	public T get() {
		synchronized (factory) {
			if (instance == null) {
				instance = factory.createInstance();
			}
			return instance;
		}
	}
}