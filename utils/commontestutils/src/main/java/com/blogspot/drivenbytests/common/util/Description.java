package com.blogspot.drivenbytests.common.util;

import java.util.Date;
import java.util.Locale;

public class Description {

  public static <T extends Number> T anyNumberWithLength(final T length) {
		return length;
	}

	public static <T extends Number> T anyNumber(final T number) {
		return number;
	}

	public static String anyString(String string) {
		return string;
	}

	public static Date anyDate(Date date) {
		return date;
	}

	@SuppressWarnings("rawtypes")
	public static <T extends Enum> T anyEnum(T enumValue) {
		return enumValue;
	}

	public static Locale anyLocale(Locale dieLocale) {
		return dieLocale;
	}
}
