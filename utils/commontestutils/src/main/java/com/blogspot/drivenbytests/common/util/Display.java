package com.blogspot.drivenbytests.common.util;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.base.Function;

public class Display {

	@SafeVarargs
	public static List<String> formatted(List<?> elemente,
			Function<?, String>... formatterFunctions) {
		final List<String> liste = new ArrayList<String>();
		final Map<Class<?>, Function<?, String>> formatter = new HashMap<Class<?>, Function<?, String>>();
		for (final Function<?, String> formatterFunction : formatterFunctions) {
			for (final Method declaredMethod: formatterFunction.getClass().getDeclaredMethods()) {
				if ("apply".equals(declaredMethod.getName())  && declaredMethod.getParameterTypes().length == 1) {
					final Class<?> forType = declaredMethod.getParameterTypes()[0];
					formatter.put(forType, formatterFunction);
				}
			}
		}
		for (Object anObject : elemente) {
			@SuppressWarnings("unchecked")
			Function<Object, String> formatterFunction = (Function<Object, String>) formatter.get(anObject.getClass());
			if (formatterFunction != null) {
				liste.add(formatterFunction.apply(anObject));
			} else {
				liste.add(String.format("No format for %s", anObject.getClass().getName()));
			}
		}
		return liste;
	}
}
