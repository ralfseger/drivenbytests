package com.blogspot.drivenbytests.jpa.util;

import java.util.Calendar;
import java.util.Date;

public class DefaultTestData {

	public Date beispielDatum() {
		final Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.YEAR, 2012);
		calendar.set(Calendar.MONTH, Calendar.APRIL);
		calendar.set(Calendar.DAY_OF_MONTH, 18);
		for (final int field : new int[] { Calendar.MILLISECOND,
				Calendar.SECOND, Calendar.MINUTE, Calendar.HOUR_OF_DAY }) {
			calendar.set(field, 0);
		}
		return calendar.getTime();
	}

}
