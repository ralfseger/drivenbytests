package com.blogspot.drivenbytests.util.web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.ErrorHandler;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.LoggerFactory;

import org.slf4j.Logger;

public interface WaitStrategy {
	void wait(final WebDriverWrapper driver, final AjaxTrigger ajaxTrigger);

	public static class ClickAndWait implements WaitStrategy {

		private static final Logger logger = LoggerFactory
				.getLogger(ClickAndWait.class);
		private final Integer timeout;

		public ClickAndWait(final int timeout) {
			this.timeout = timeout;

		}

		public void wait(WebDriverWrapper driver, AjaxTrigger ajaxTrigger) {
			ajaxTrigger.invoke();
			// wait until ajax trigger has been removed
			logger.info("waiting " + timeout
					+ " ms for ajax to refresh page ...");
			synchronized (timeout) {
				try {
					timeout.wait(timeout);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}

	public static class WaitWithHiddenElement implements WaitStrategy {

		private static final Logger logger = LoggerFactory
				.getLogger(WaitWithHiddenElement.class);
		private static final String ID = "id";
		private static final String MARK = "marked";
		private final String xpathId;

		public WaitWithHiddenElement(final String xpathId) {
			this.xpathId = xpathId;
		}

		public void wait(WebDriverWrapper driver, AjaxTrigger ajaxTrigger) {
			DriverUtil.createDivUnderXpathElem(driver.getDriver(), xpathId, MARK);
			ajaxTrigger.invoke();
			WebDriverWait waiter = new WebDriverWait(driver.getDriver(), 5000);
			waiter.until(new ExpectedCondition<WebElement>() {
				public WebElement apply(WebDriver driver) {
					WebElement innerDiv = null;
					for (final WebElement td : driver.findElements(By
							.xpath(xpathId))) {
						try {
							for (final WebElement anInnerDiv : td
									.findElements(By.tagName(DriverUtil.DIV))) {
								if (MARK.equals(anInnerDiv.getAttribute(ID))) {
									innerDiv = anInnerDiv;
								}
							}
						} catch (final WebDriverException x) {
							if (x instanceof ErrorHandler.UnknownServerException) {
								final String errorMsg = x.getMessage();
								if (errorMsg.contains("no longer attached")) {
									logger.debug(x.getMessage());
								} else {
									logger.warn("webdriver exception", x);
								}
							} else {
								logger.warn("webdriver exception", x);
							}
						}
					}
					if (innerDiv == null) {
						return driver.findElement(By.tagName(DriverUtil.BODY));
					}
					logger.info("found marked element -> resume waiting");
					return null;
				}
			});
		}

	}
}
