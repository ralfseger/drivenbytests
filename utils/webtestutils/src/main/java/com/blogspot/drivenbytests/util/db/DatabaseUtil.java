package com.blogspot.drivenbytests.util.db;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.dbunit.DatabaseUnitException;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.database.QueryDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.operation.DatabaseOperation;

public class DatabaseUtil {

	private static final String DRIVER_CLASS_NAME = "driverClassName";
	private static final String URL = "url";
	private static final String USER = "user";
	private static final String PASSWORD = "password";

	private static DatabaseUtil instance = new DatabaseUtil();

	private DatabaseUtil() {
	}

	public static DatabaseUtil getInstance() {
		return instance;
	}

	public void resetTo(final Class<?> resourceHolder)
			throws DatabaseUnitException, SQLException {
		IDatabaseConnection connection = null;
		try {
			connection = new DatabaseConnection(createJdbcConnection());
			// ...
//TODO test!
			// initialize your dataset here
			IDataSet dataSet = null;
			// ...

			DatabaseOperation.CLEAN_INSERT.execute(connection, dataSet);
		} finally {

			connection.close();
		}
	}

	public void exportDatabaseTables(final OutputStream targetStream,
			String... args) throws DatabaseUnitException, SQLException,
			IOException {
		final Connection jdbcConnection = createJdbcConnection();
		final IDatabaseConnection connection = new DatabaseConnection(
				jdbcConnection);
		final QueryDataSet partialDataSet = new QueryDataSet(connection);
		String schemaPattern = (args != null && args.length > 0) ? args[0]
				: null;
		final ResultSet tables = jdbcConnection.getMetaData().getTables(null,
				schemaPattern.toUpperCase(), "%", new String[] { "TABLE" });
		while (tables.next()) {
			partialDataSet.addTable(tables.getString(3));

		}
		FlatXmlDataSet.write(partialDataSet, targetStream);
	}

	private Connection createJdbcConnection() throws SQLException {
		final Properties dbProps = new Properties();
		try {
			dbProps.load(DatabaseUtil.class
					.getResourceAsStream("/config/db.properties"));
			@SuppressWarnings("unchecked")
			final Class<Driver> driverClass = (Class<Driver>) Class
					.forName(dbProps.getProperty(DRIVER_CLASS_NAME));
			final Driver driver = driverClass.newInstance();
			DriverManager.registerDriver(driver);
			return DriverManager.getConnection(dbProps.getProperty(URL),
					dbProps.getProperty(USER), dbProps.getProperty(PASSWORD));
		} catch (final SQLException se) {
			throw se;
		} catch (final Exception x) {
			throw new RuntimeException(x);
		}
	}

	private static String escape(String separator) {
		if (separator.equals("\\")) {
			return "\\\\";
		} else {
			return separator;
		}
	}

	private void exportDb(final String fileName, final String... args) {
		final File target = FileUtils.getFile(fileName
				.split(escape(File.separator)));
		target.getParentFile().mkdirs();
		FileOutputStream targetStream = null;
		try {
			targetStream = new FileOutputStream(target);
			exportDatabaseTables(targetStream, args);
		} catch (final Exception x) {
			x.printStackTrace();
		} finally {
			if (targetStream != null) {
				try {
					targetStream.flush();
					targetStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static final void main(final String[] args) {
		if (args.length < 2) {
			System.out
					.println("usage java DatabaseUtil [export|import] {targetFile} <schemaPattern>");
			System.exit(-1);
		}
		if (args[0].toLowerCase().startsWith("exp")) {
			final String[] userArgs = new String[args.length - 2];
			for (int i = 0; i < userArgs.length; i++) {
				userArgs[i] = args[i + 2];
			}
			getInstance().exportDb(args[1], userArgs);
		}
	}

}
