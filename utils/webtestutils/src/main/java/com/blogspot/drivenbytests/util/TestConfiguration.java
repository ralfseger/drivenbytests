package com.blogspot.drivenbytests.util;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.blogspot.drivenbytests.util.web.ConfiguredTestPage;

public class TestConfiguration {

	private static final String LAST_PAGE_ELEMENT_XPATH_EXPR = "last-page-element";
	private static final String FACES_ROOT_URL = "faces-root-url";
	public static final int TIMEOUT = 5000;

	private static final Logger logger = LoggerFactory
			.getLogger(TestConfiguration.class);
	private final WebDriver driver;
	private final Properties properties;

	private static TestConfiguration configuration = new TestConfiguration();

	private TestConfiguration() {
		properties = new Properties();
		try {
			properties.load(TestConfiguration.class
					.getResourceAsStream("/config/adf-config.properties"));
			@SuppressWarnings("unchecked")
			final Class<WebDriver> webdriverClassName = (Class<WebDriver>) Class
					.forName(properties.getProperty("webdriver-class"));
			driver = webdriverClassName.newInstance();
			logger.info("configured webdriver : "+driver);
		} catch (final Exception io) {
			throw new RuntimeException(
					"could not load required properties \"adf-config.properties\"",
					io);
		}
	}

	public static TestConfiguration getInstance() {
		return configuration;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public <PAGETYPE> PAGETYPE loadPage(Class<PAGETYPE> pageClass,
			final String pageName) {
		openPage(pageName);
		final String lastPageElementXpathExpr = properties
				.getProperty(LAST_PAGE_ELEMENT_XPATH_EXPR);
		logger.info("waiting for page to be rendered ...");
		final WebDriverWait waiter = new WebDriverWait(driver, TIMEOUT);
		waiter.until(new ExpectedCondition<WebElement>() {
			public WebElement apply(WebDriver driver) {
				return driver.findElement(By.xpath(lastPageElementXpathExpr));
			}
		});
		final PAGETYPE page = PageFactory.initElements(driver, pageClass);
		if (page instanceof ConfiguredTestPage) {
			((ConfiguredTestPage) page).setDriver(driver);
		}
		return page;
	}

	public void cleanUp() {
		driver.close();
	}

	private void openPage(String pageName) {
		final StringBuilder fullPage = new StringBuilder();
		final String basePage = properties.getProperty(FACES_ROOT_URL);
		fullPage.append(basePage);
		if (!basePage.endsWith("/")) {
			fullPage.append("/");
		}
		fullPage.append(pageName);
		driver.get(fullPage.toString());

	}
}
