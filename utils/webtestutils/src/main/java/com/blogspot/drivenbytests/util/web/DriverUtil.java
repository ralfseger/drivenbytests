package com.blogspot.drivenbytests.util.web;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DriverUtil {

	private static final Logger logger = LoggerFactory
			.getLogger(DriverUtil.class);
	static String BODY = "body";
	static String DIV = "div";
	
	public static void createDivUnderXpathElem(final WebDriver driver,
			final String xpathElem, final String newId) {
		logger.info("adding artificial element for ajax verification");
		final Object result = ((JavascriptExecutor) driver)
				.executeScript("try {"
						+ "  var child = document.createElement('"
						+ DIV
						+ "'); "
						+ "  child.setAttribute('id','"
						+ newId
						+ "'); "
						+ "  var parentByXpath = document.evaluate(\""
						+ xpathElem
						+ "\", document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null);"
						+ "  parentByXpath.singleNodeValue.appendChild(child)"
						+ "} catch (e) {return e;}");
		if (result != null) {
			logger.info("adding artificial element for ajax verification returns exception:"
					+ result);
		}
	}

	

	
}
