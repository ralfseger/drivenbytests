package com.blogspot.drivenbytests.util.web;

import org.openqa.selenium.WebDriver;


public abstract class ConfiguredTestPage {

	protected WebDriverWrapper driver;

	public void setDriver(final WebDriver driver) {
		this.driver = new WebDriverWrapper(driver);
	}
}
