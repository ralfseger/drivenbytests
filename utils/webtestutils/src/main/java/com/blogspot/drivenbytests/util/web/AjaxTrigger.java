package com.blogspot.drivenbytests.util.web;

import org.openqa.selenium.WebElement;

public interface AjaxTrigger {

	void invoke();
	
	static class ClickableTrigger implements AjaxTrigger {

		private final WebElement clickableTrigger;

		ClickableTrigger(final WebElement clickableTrigger) {
			this.clickableTrigger = clickableTrigger;
		}
		
		public void invoke() {
			clickableTrigger.click();
		}
		
	}
}
