package com.blogspot.drivenbytests.util.web;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebDriverWrapper {

	private final WebDriver driver;
	private static final Logger logger = LoggerFactory
			.getLogger(WebDriverWrapper.class);

	WebDriverWrapper(final WebDriver driver) {
		this.driver = driver;
	}

	public void setListByName(final WebElement listElementHolder, final String name, final String targetElementXpath) {
		WebElement toBeClicked = null;

		for (WebElement listItem : listElementHolder.findElements(By
				.tagName("li"))) {
			if (name.equals(listItem.getText())) {
				toBeClicked = listItem;
			}

		}
		if (toBeClicked != null) {
			new WaitStrategy.WaitWithHiddenElement(targetElementXpath).wait(this,
					new AjaxTrigger.ClickableTrigger(toBeClicked));
		}
		logger.info("department name : " + name
				+ (toBeClicked == null ? "not" : "")
				+ " set.");
	}

	WebDriver getDriver() {
		return driver;
	}

}
