package com.blogspot.drivenbytests.jpa.util;

import static org.mockito.Mockito.mock;

import java.util.List;

import javax.persistence.EntityManager;

import org.junit.Before;

public abstract class AbstractJPAEntityManagerTest<TESTSERVICE, TESTDATA extends DefaultTestData, FORMATTERS extends DefaultFormatter> {

	protected EntityManager emMock;
	protected TESTSERVICE service; 
	protected TESTDATA testData;
	protected FORMATTERS formatters;
	private EntityManagerTestUtil testUtil;

	@Before
	public void setUp() {
		testData = newTestData();
		emMock = mock(EntityManager.class);
		formatters = newFormatters();
		testUtil = new EntityManagerTestUtil();
		testUtil.initSequenceMock(emMock);
		service = initServiceImpl(emMock);
	}

	protected abstract TESTSERVICE initServiceImpl(final EntityManager entityMgrMock);

	protected abstract TESTDATA newTestData();

	protected abstract FORMATTERS newFormatters();

	protected <T> void defaultQueryMitAntwort(final List<T> ergebnisListeEin,
			Class<T> ergebnisTyp) {
		testUtil.defaultQueryMitAntwort(emMock, ergebnisListeEin, ergebnisTyp);
	}

	protected <T> void countQueryMitAntwort(final List<T> ergebnisListeEin,
			Class<T> ergebnisTyp) {
		testUtil.countQueryMitAntwort(emMock, ergebnisListeEin, ergebnisTyp);
	}

}