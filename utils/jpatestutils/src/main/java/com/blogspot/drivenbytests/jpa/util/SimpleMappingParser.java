package com.blogspot.drivenbytests.jpa.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

public class SimpleMappingParser {

	public void addParamaterMappings(String query, Map<String, String> queryParameterMapping) {
		final List<String> tokenCollection = new ArrayList<>();
		for (final String part : query.split("\\s")) {
			final StringTokenizer tokenizer = new StringTokenizer(part, " =<>", true);
			while (tokenizer.hasMoreTokens()) {
				tokenCollection.add(tokenizer.nextToken());
			}
		}
		for (int i = 2; i < tokenCollection.size(); i++) {
			final String token = tokenCollection.get(i);
			if (token.startsWith(":")) {
				if (isOperator(tokenCollection.get(i - 1))) {
					queryParameterMapping.put(trimColon(token), trimDot(tokenCollection.get(i - 2)));
				} else if (isOperator(tokenCollection.get(i + 1))) {
					queryParameterMapping.put(trimColon(token), trimDot(tokenCollection.get(i + 2)));
				} else {
					throw new IllegalArgumentException(String.format("cannot find operator in Query %s", query));
				}
			}
		}

	}

	private String trimDot(String string) {
		final int dotIdx = string.indexOf(".");
		if (dotIdx<1) {
			return string;
		}
		return string.substring(1 + dotIdx);
	}

	private String trimColon(String token) {
		return token.substring(1);
	}

	private boolean isOperator(String tokenBefore) {
		return "=".equals(tokenBefore);
	}

}
