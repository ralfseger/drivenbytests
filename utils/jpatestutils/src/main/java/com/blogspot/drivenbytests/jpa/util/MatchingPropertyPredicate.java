package com.blogspot.drivenbytests.jpa.util;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Predicate;

public class MatchingPropertyPredicate<T> implements Predicate<T> {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(MatchingPropertyPredicate.class);
	private final String key;
	private final Object value;

	public MatchingPropertyPredicate(String key, Object value) {
		LOGGER.debug(String.format("new MatchPredicate %s.%s)", key, value));
		this.key = key;
		this.value = value;
	}

	@Override
	public boolean apply(T input) {
		try {
			final Object propertyValue = PropertyUtils.getProperty(input, key);
			LOGGER.debug(String
					.format("check %s.%s for equality (reference=%s,property=%s)",
							input.getClass().getSimpleName(), key, value,
							propertyValue));
			boolean gleicht = new EqualsBuilder().append(value, propertyValue)
					.isEquals();
			return gleicht;
		} catch (Exception e) {
			throw new RuntimeException(String.format(
					"Kann auf %s keine Eigenschaft %s finden", input, key), e);
		}
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this,
				ToStringStyle.SHORT_PREFIX_STYLE);
	}

}