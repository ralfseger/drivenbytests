package com.blogspot.drivenbytests.jpa.util;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.persistence.descriptors.ClassDescriptor;
import org.eclipse.persistence.internal.databaseaccess.DatasourcePlatform;
import org.eclipse.persistence.internal.descriptors.InitObjectBuilder;
import org.eclipse.persistence.internal.helper.ConversionManager;
import org.eclipse.persistence.internal.helper.JPAConversionManager;
import org.eclipse.persistence.internal.jpa.JPAQuery;
import org.eclipse.persistence.internal.jpa.jpql.HermesParser;
import org.eclipse.persistence.internal.sessions.AbstractSession;
import org.eclipse.persistence.queries.JPAQueryBuilder;
import org.eclipse.persistence.sessions.Project;
import org.eclipse.persistence.sessions.Session;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;

public class JPAParameterMappingUtil {

	public static boolean VALIDATE_MAPPING = false;
	private static JPAParameterMappingUtil instance = new JPAParameterMappingUtil();
	private Map<String, ClassDescriptor> knownDescriptors = new HashMap<String, ClassDescriptor>();

	protected JPAParameterMappingUtil() {
	}

	public static JPAParameterMappingUtil getInstance() {
		return instance;
	}

	public void registerEntityClass(final Class<?> entityClass) {
		if (knownDescriptors.containsKey(entityClass.getSimpleName())) {
			return;
		}
		knownDescriptors.put(entityClass.getSimpleName(), descriptorFrom(entityClass));
	}

	public void provideParameterMapping(final String query, final Map<String, String> queryParameterMapping) {
		if (query != null && queryParameterMapping.isEmpty()) {
			if (VALIDATE_MAPPING) {
				validateJPAQuery(query);
			}
			new SimpleMappingParser().addParamaterMappings(query, queryParameterMapping);
		}
	}

	private void validateJPAQuery(final String query) {
		final JPAQuery jpaQuery = new JPAQuery(query);
		final Session session = mockJPASession();
		jpaQuery.processJPQLQuery(session);
	}

	private Session mockJPASession() {
		final AbstractSession session = mock(AbstractSession.class);
		final DatasourcePlatform platform = mock(DatasourcePlatform.class);
		final ConversionManager conversionManager = new JPAConversionManager();
		when(session.getDatasourcePlatform()).thenReturn(platform);
		when(platform.getConversionManager()).thenReturn(conversionManager);
		final Project project = new Project();
		when(session.getProject()).thenReturn(project);
		when(session.getDescriptorForAlias(anyString())).thenAnswer(new Answer<ClassDescriptor>() {
			@Override
			public ClassDescriptor answer(InvocationOnMock invocation) throws Throwable {
				final String className = (String) invocation.getArguments()[0];
				return determineDescriptorFor(className);
			}
		});
		when(session.getDescriptor(any(Class.class))).thenAnswer(new Answer<ClassDescriptor>() {

			@Override
			public ClassDescriptor answer(InvocationOnMock invocation) throws Throwable {
				final Class<?> theClass = (Class<?>) invocation.getArguments()[0];
				return determineDescriptorFor(theClass.getSimpleName());
			}
		});
		JPAQueryBuilder queryBuilder = new HermesParser();
		when(session.getQueryBuilder()).thenReturn(queryBuilder);
		return session;
	}

	private ClassDescriptor determineDescriptorFor(final String className) {
		if (knownDescriptors.containsKey(className)) {
			return knownDescriptors.get(className);
		}
		throw new IllegalArgumentException(String.format("unknown className %s", className));
	}

	private ClassDescriptor descriptorFrom(Class<?> entityClass) {
		final ClassDescriptor descriptor = new ClassDescriptor();
		descriptor.setJavaClass(entityClass);
		InitObjectBuilder.getInstance().setFieldMappingsFor(descriptor.getObjectBuilder(), entityClass);
		return descriptor;
	}

}
