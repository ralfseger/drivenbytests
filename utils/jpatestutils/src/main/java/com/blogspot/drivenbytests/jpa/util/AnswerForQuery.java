package com.blogspot.drivenbytests.jpa.util;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

public class AnswerForQuery<T> implements Answer<List<T>> {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(AnswerForQuery.class);
	private final List<T> ergebnisse;
	private final Map<String, Object> parameters;
	private final Map<String, String> queryParameterMapping;
	private String query;

	public AnswerForQuery(final TypedQuery<T> query, final List<T> ergebnisse) {
		this.ergebnisse = ergebnisse;
		this.queryParameterMapping = new HashMap<>();		
		parameters = new HashMap<String, Object>();
		when(query.setParameter(anyString(), any())).thenAnswer(
				new Answer<TypedQuery<T>>() {
					@Override
					public TypedQuery<T> answer(InvocationOnMock invocation)
							throws Throwable {
						parameters.put((String) invocation.getArguments()[0],
								invocation.getArguments()[1]);
						return query;
					}
				});
	}

	@Override
	public List<T> answer(final InvocationOnMock invocation) throws Throwable {
		final String normalizedQuery = query.toLowerCase();
		Iterable<T> matches = Iterables.filter(ergebnisse, new Predicate<T>() {
			@Override
			public boolean apply(T input) {
				if (input == null) {
					throw new IllegalArgumentException("cannot process Collections containing NULL");
				}
				Predicate<T> alleEigenschaftenStimmenUeberein = Predicates
						.alwaysTrue();
				for (final Map.Entry<String, Object> parameter : parameters
						.entrySet()) {
					String key = parameter.getKey();
					JPAParameterMappingUtil.getInstance().provideParameterMapping(query, queryParameterMapping);
					if (queryParameterMapping.containsKey(key)) {
						key = queryParameterMapping.get(key);
					}
					boolean connectByAnd = true;
					boolean argFound = false;
					String queryArg = ":" + key;
					int index = query.indexOf(parameter.getKey());
					while (index > -1) {
						argFound = true;
						if (normalizedQuery.lastIndexOf(" or ") > normalizedQuery
								.lastIndexOf(" and ")) {
							connectByAnd = false;
						}
						index = query.indexOf(queryArg, 1 + index);
					}
					if (connectByAnd && argFound) {
						final Predicate<T> matchingPropertyPredicate = new MatchingPropertyPredicate<T>(
								key, parameter.getValue());
						alleEigenschaftenStimmenUeberein = Predicates.<T> and(
								alleEigenschaftenStimmenUeberein,
								matchingPropertyPredicate);
					} else {
						LOGGER.warn(String
								.format("JPA Query(%s) eigenschaft '%s' nicht gepr�ft.",
										query, key));
					}
				}
				return alleEigenschaftenStimmenUeberein.apply(input);
			}

			
		});
		return Lists.newArrayList(matches);
	}

	public void setQuery(String query) {
		this.query = query;
	}
}