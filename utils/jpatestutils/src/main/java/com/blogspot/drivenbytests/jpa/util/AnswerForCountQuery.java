package com.blogspot.drivenbytests.jpa.util;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.TypedQuery;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Predicate;
import com.google.common.base.Predicates;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;

public class AnswerForCountQuery<T> implements Answer<Number> {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(AnswerForCountQuery.class);
	private final List<T> ergebnisse;
	private final Map<String, Object> parameters;
	private final Map<String, String> queryParameterMapping;

	public AnswerForCountQuery(final TypedQuery<Number> query,
			final List<T> ergebnisse) {
		this.ergebnisse = ergebnisse;
		this.queryParameterMapping = new HashMap<>();
		parameters = new HashMap<String, Object>();
		when(query.setParameter(anyString(), any())).thenAnswer(
				new Answer<TypedQuery<Number>>() {
					@Override
					public TypedQuery<Number> answer(InvocationOnMock invocation)
							throws Throwable {
						parameters.put((String) invocation.getArguments()[0],
								invocation.getArguments()[1]);
						return query;
					}
				});
	}

	@Override
	public Number answer(InvocationOnMock invocation) throws Throwable {
		final Iterable<T> matches = Iterables.filter(ergebnisse,
				new Predicate<T>() {
					@Override
					public boolean apply(T input) {
						List<Predicate<T>> alleEigenschaftenStimmenUeberein = new ArrayList<Predicate<T>>();
						for (final Map.Entry<String, Object> parameter : parameters
								.entrySet()) {
							String key = parameter.getKey();
							if (queryParameterMapping.containsKey(key)) {
								key = queryParameterMapping.get(key);
							}
							final Predicate<T> matchingPropertyPredicate = new MatchingPropertyPredicate<T>(
									key, parameter.getValue());
							alleEigenschaftenStimmenUeberein
									.add(matchingPropertyPredicate);
						}
						return Predicates.and(alleEigenschaftenStimmenUeberein)
								.apply(input);
					}
				});
		final int size = Lists.newArrayList(matches).size();
		LOGGER.info(String.format("query for [%s] returns %d results",
				parameters, size));
		return size;
	}

}