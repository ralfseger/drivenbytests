package com.blogspot.drivenbytests.jpa.util;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.contains;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class EntityManagerTestUtil {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(EntityManagerTestUtil.class);
	private static int sequenceNr = 10000;
	
	public <T> void defaultQueryMitAntwort(final EntityManager emMock, final List<T> ergebnisListeEin,
			Class<T> ergebnisTyp) {
		@SuppressWarnings("unchecked")
		final TypedQuery<T> query = mock(TypedQuery.class);
		final List<T> ergebnisListe = new ArrayList<T>(ergebnisListeEin);
		final AnswerForQuery<T> answer = new AnswerForQuery<T>(query,
				ergebnisListe);
		fluentQuery(query);		
		when(query.getResultList()).thenAnswer(answer);
		when(emMock.createQuery(anyString(), eq(ergebnisTyp))).thenAnswer(
				new Answer<TypedQuery<T>>() {
					@Override
					public TypedQuery<T> answer(InvocationOnMock invocation)
							throws Throwable {
						answer.setQuery((String) invocation.getArguments()[0]);
						LOGGER.info(String.format(
								"defaultQueryMitAntwort('%s')",
								invocation.getArguments()[0]));
						return query;
					}
				});
		initDynamicResults(emMock, ergebnisTyp, ergebnisListe);
	}
	
	public <T> void countQueryMitAntwort(final EntityManager emMock, final List<T> ergebnisListeEin,
			Class<T> ergebnisTyp) {
		@SuppressWarnings("unchecked")
		final TypedQuery<Number> query = mock(TypedQuery.class);
		final List<T> ergebnisListe = new ArrayList<T>(ergebnisListeEin);
		final Answer<Number> countAnswer = new AnswerForCountQuery<T>(query,
				ergebnisListe);
		fluentQuery(query);
		when(query.getSingleResult()).thenAnswer(countAnswer);
		when(
				emMock.createQuery(contains(ergebnisTyp.getSimpleName()),
						eq(Number.class))).thenAnswer(
				new Answer<TypedQuery<Number>>() {
					@Override
					public TypedQuery<Number> answer(InvocationOnMock invocation)
							throws Throwable {
						LOGGER.info(String.format("countQueryMitAntwort('%s')",
								invocation.getArguments()[0]));
						return query;
					}
				});
		initDynamicResults(emMock, ergebnisTyp, ergebnisListe);
	}
	
	public void initSequenceMock(final EntityManager emMock) {
		Query query = mock(Query.class);
		when(emMock.createNativeQuery(contains("nextval"))).thenReturn(query);
		fluentQuery(query);
		when(query.getSingleResult()).thenAnswer(new Answer<Number>() {
			@Override
			public Number answer(InvocationOnMock invocation) throws Throwable {
				LOGGER.info("sequenceQuery");
				return sequenceNr++;
			}
		});
	}
	
	private <T> void initDynamicResults(final EntityManager emMock,
			Class<T> ergebnisTyp, final List<T> ergebnisListe) {
		doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				@SuppressWarnings("unchecked")
				final T toPersist = (T) invocation.getArguments()[0];
				if (!ergebnisListe.contains(toPersist)) {
					ergebnisListe.add(toPersist);
				}
				return null;
			}
			
		}).when(emMock).persist(isA(ergebnisTyp));
		doAnswer(new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				@SuppressWarnings("unchecked")
				final T toDelete = (T) invocation.getArguments()[0];
				if (ergebnisListe.contains(toDelete)) {
					ergebnisListe.remove(toDelete);
				}
				return null;
			}
		}).when(emMock).remove(isA(ergebnisTyp));
	}
	
	private void fluentQuery(Query query) {
		when(query.setMaxResults(anyInt())).thenReturn(query);
	}
}
