package org.eclipse.persistence.internal.descriptors;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Id;

import org.eclipse.persistence.descriptors.ClassDescriptor;
import org.eclipse.persistence.mappings.DatabaseMapping;

public class InitObjectBuilder {

	private static InitObjectBuilder instance = new InitObjectBuilder();

	protected InitObjectBuilder() {}
	
	public static InitObjectBuilder getInstance() {
		return instance ;
	}

	public void setFieldMappingsFor(ObjectBuilder objectBuilder, Class<?> entityClass) {
		final Map<String, DatabaseMapping> theAttributeMappings = new HashMap<String, DatabaseMapping>();
		for (final Field field: entityClass.getDeclaredFields()) {
			Column column = field.getAnnotation(Column.class);
			if (column != null) {
				theAttributeMappings.put(field.getName(), mockDbMapping(field));
			}
			Id id = field.getAnnotation(Id.class);
			if (id != null) {
				theAttributeMappings.put(field.getName(), mockDbMapping(field));
			}
		}
		objectBuilder.setMappingsByAttribute(theAttributeMappings );
	}

	private DatabaseMapping mockDbMapping(final Field field) {
		final DatabaseMapping mockDbMapping = mock(DatabaseMapping.class);
		final ClassDescriptor mockClassDescriptor = new ClassDescriptor();
		when(mockDbMapping.getReferenceDescriptor()).thenReturn(mockClassDescriptor);
		final InstanceVariableAttributeAccessor attributeAccessor = mock(InstanceVariableAttributeAccessor.class);
		when(mockDbMapping.getAttributeAccessor()).thenReturn(attributeAccessor);
		when(attributeAccessor.isInstanceVariableAttributeAccessor()).thenReturn(true);
		when(attributeAccessor.getAttributeField()).thenReturn(field);
		return mockDbMapping;
	}

}
