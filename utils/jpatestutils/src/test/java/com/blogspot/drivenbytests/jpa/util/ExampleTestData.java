package com.blogspot.drivenbytests.jpa.util;

import com.blogspot.drivenbytests.jpa.domain.Edge;
import com.blogspot.drivenbytests.jpa.domain.Graph;
import com.blogspot.drivenbytests.jpa.domain.Value;
import com.blogspot.drivenbytests.jpa.domain.Vertex;

import com.blogspot.drivenbytests.jpa.util.DefaultTestData;

public class ExampleTestData extends DefaultTestData {

	public Graph petersenGraph() {
		final Graph graph = new Graph();
		final Vertex v1 = new Vertex(value("v1"));
		final Vertex v2 = new Vertex(value("v2"));
		final Vertex v3 = new Vertex(value("v3"));
		final Vertex v4 = new Vertex(value("v4"));
		final Vertex v5 = new Vertex(value("v5"));
		final Vertex v6 = new Vertex(value("v6"));
		final Vertex v7 = new Vertex(value("v7"));
		final Vertex v8 = new Vertex(value("v8"));
		final Vertex v9 = new Vertex(value("v9"));
		final Vertex v10 = new Vertex(value("v10"));
		new Edge(v1, v2, graph);
		new Edge(v1, v4, graph);
		new Edge(v1, v10, graph);
		new Edge(v2, v6, graph);
		new Edge(v2, v7, graph);
		new Edge(v3, v4, graph);
		new Edge(v3, v7, graph);
		new Edge(v3, v9, graph);
		new Edge(v4, v5, graph);
		new Edge(v5, v6, graph);
		new Edge(v5, v8, graph);
		new Edge(v6, v9, graph);
		new Edge(v7, v8, graph);
		new Edge(v8, v10, graph);
		new Edge(v9, v10, graph);
		return graph;
	}

	private Value value(String stringValue) {
		final Value value = new Value();
		value.setString(stringValue);
		return value;
	}
}
