package com.blogspot.drivenbytests.jpa.util;

import javax.persistence.EntityManager;
import com.blogspot.drivenbytests.jpa.util.DefaultTestData;
import com.blogspot.drivenbytests.jpa.util.DefaultFormatter;

public class JPAEntityManagerSpec extends AbstractJPAEntityManagerTest<ExampleBusinessService, DefaultTestData, DefaultFormatter>{

	@Override
	protected ExampleBusinessService initServiceImpl(EntityManager entityMgrMock) {
		final ExampleBusinessService businessService = new ExampleBusinessService();
		return businessService;
	}	

	@Override
	protected DefaultTestData newTestData() {
		return new ExampleTestData();
	}

	@Override
	protected DefaultFormatter newFormatters() {
		return new ExampleTestFormatters();
	}

	
	

}
