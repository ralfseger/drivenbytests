package com.blogspot.drivenbytests.jpa.util;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


public class SimpleMappingParserTest {

	private Map<String, String> mappings;
	private SimpleMappingParser parser;
	
	@Before
	public void init() {
		this.parser = new SimpleMappingParser();
		mappings = new HashMap<String, String>();
	}
	
	@Test
	public void noParamaterResultsInEmptyMap() {
		parser.addParamaterMappings("select e from ExampleEntity e", mappings);
		assertThat(mappings.keySet(), hasSize(0));
	}
	
	@Test
	public void simpleMappingIsFound() {
		parser.addParamaterMappings("select ee from ExampleEntity ee where ee.stringValue=:string", mappings);
		assertThat(mappings, equalTo(Collections.singletonMap("string", "stringValue")));
	}
	
	@Test
	public void reverseSimpleMappingIsFound() {
		parser.addParamaterMappings("select ee from ExampleEntity ee where :string=ee.stringValue", mappings);
		assertThat(mappings, equalTo(Collections.singletonMap("string", "stringValue")));
	}
}
