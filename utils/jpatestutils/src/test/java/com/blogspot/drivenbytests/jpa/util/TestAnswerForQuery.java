package com.blogspot.drivenbytests.jpa.util;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

import java.util.Collections;
import java.util.List;

import javax.persistence.EntityManager;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.blogspot.drivenbytests.jpa.domain.ExampleEntity;

public class TestAnswerForQuery {

	private ExampleEntityTestData testData;
	private EntityManagerTestUtil testUtil;
	private EntityManager entityManager;
	
	@BeforeClass
	public static void setupStatic() {
		JPAParameterMappingUtil.getInstance().registerEntityClass(ExampleEntity.class);
	}
	
	@Before
	public void setUp() {
		testData = new ExampleEntityTestData();
		testUtil = new EntityManagerTestUtil();
		entityManager = mock(EntityManager.class);
	}
	
	@Test
	public void noEntityIsFoundBeforePersistOrInit() {
		initEntityManager(Collections.<ExampleEntity>emptyList());
		List<ExampleEntity> resultList = entityManager.createQuery("select ee from ExampleEntity ee order by ee.id", ExampleEntity.class).getResultList();
		assertThat(resultList, hasSize(0));
	}
	
	@Test
	public void entityIsFoundAfterPersist() {
		initEntityManager(Collections.<ExampleEntity>emptyList());
		entityManager.persist(testData.exampleEntity());
		List<ExampleEntity> resultList = entityManager.createQuery("select ee from ExampleEntity ee order by ee.id", ExampleEntity.class).getResultList();
		assertThat(resultList, Matchers.<List<ExampleEntity>>both(hasSize(1)).and(contains(equalTo(testData.exampleEntity()))));
	}
	

	@Test
	public void entityIsFoundAfterInit() {
		initEntityManager(Collections.<ExampleEntity>singletonList(testData.exampleEntity()));
		List<ExampleEntity> resultList = entityManager.createQuery("select ee from ExampleEntity ee order by ee.id", ExampleEntity.class).getResultList();
		assertThat(resultList, Matchers.<List<ExampleEntity>>both(hasSize(1)).and(contains(equalTo(testData.exampleEntity()))));
	}
	
	@Test
	public void entityWithUnmatchedParameterIsNotFound() {
		initEntityManager(Collections.<ExampleEntity>singletonList(testData.exampleEntity()));
		List<ExampleEntity> resultList = entityManager.createQuery("select ee from ExampleEntity ee where ee.stringValue=:string order by ee.id", ExampleEntity.class)
				.setParameter("string", testData.exampleEntity().getStringValue()+".modified")
				.getResultList();
		assertThat(resultList, hasSize(0));
	}
	
	@Test
	public void entityWithMatchedParameterIsNotFound() {		
		initEntityManager(Collections.<ExampleEntity>singletonList(testData.exampleEntity()));
		List<ExampleEntity> resultList = entityManager.createQuery("select ee from ExampleEntity ee where ee.stringValue=:string order by ee.id", ExampleEntity.class)
				.setParameter("string", testData.exampleEntity().getStringValue())
				.getResultList();
		assertThat(resultList, Matchers.<List<ExampleEntity>>both(hasSize(1)).and(contains(equalTo(testData.exampleEntity()))));
	}
	
	
	// -- helper
	
	private void initEntityManager(List<ExampleEntity> results) {
		testUtil.defaultQueryMitAntwort(entityManager, results, ExampleEntity.class);
	}
}
