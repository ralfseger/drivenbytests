package com.blogspot.drivenbytests.jpa.util;

import static com.blogspot.drivenbytests.common.util.Description.anyNumber;
import static com.blogspot.drivenbytests.common.util.Description.anyString;

import com.blogspot.drivenbytests.jpa.domain.ExampleEntity;
public class ExampleEntityTestData {

	public ExampleEntity exampleEntity() {
		final ExampleEntity ee = new ExampleEntity();
		ee.setId(anyNumber(42l));
		ee.setLongValue(anyNumber(314159265359l));
		ee.setStringValue(anyString("Pi"));
		return ee;
	}
}
