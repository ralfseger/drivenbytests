package com.blogspot.drivenbytests.jpa.ejbutil.conversion;

import java.math.BigDecimal;

public class BigDecimalToIntegerConverter implements
		TypeConverter<BigDecimal, Integer> {

	@Override
	public Integer convertedFrom(BigDecimal from, Class<?> toType) {
		return from.intValue();
	}

}
