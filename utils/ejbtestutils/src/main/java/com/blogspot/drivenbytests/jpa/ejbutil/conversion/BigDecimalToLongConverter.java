package com.blogspot.drivenbytests.jpa.ejbutil.conversion;

import java.math.BigDecimal;

public class BigDecimalToLongConverter implements
		TypeConverter<BigDecimal, Long> {

	@Override
	public Long convertedFrom(BigDecimal from, Class<?> toType) {
		return from.longValue();
	}

}
