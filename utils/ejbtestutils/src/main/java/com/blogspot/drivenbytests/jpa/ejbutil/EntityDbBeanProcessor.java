package com.blogspot.drivenbytests.jpa.ejbutil;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.JoinColumn;

import org.apache.commons.dbutils.BeanProcessor;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.blogspot.drivenbytests.jpa.ejbutil.conversion.BigDecimalToEnumConverter;
import com.blogspot.drivenbytests.jpa.ejbutil.conversion.BigDecimalToIntegerConverter;
import com.blogspot.drivenbytests.jpa.ejbutil.conversion.BigDecimalToLongConverter;
import com.blogspot.drivenbytests.jpa.ejbutil.conversion.TypeConverter;

public class EntityDbBeanProcessor extends BeanProcessor {

	private static final Logger LOGGER = LoggerFactory
			.getLogger(EntityDbBeanProcessor.class);
	private final Map<Class<?>, Map<Class<?>, TypeConverter<?, ?>>> converterMap = initConverterMap();

	@Override
	public <T> List<T> toBeanList(ResultSet rs, Class<T> type)
			throws SQLException {
		List<T> results = new ArrayList<T>();

		if (!rs.next()) {
			return results;
		}
		do {
			results.add(this.createBean(rs, type));
		} while (rs.next());

		return results;
	}

	private Map<Class<?>, Map<Class<?>, TypeConverter<?, ?>>> initConverterMap() {
		final Map<Class<?>, Map<Class<?>, TypeConverter<?, ?>>> knownConverterMap = new HashMap<Class<?>, Map<Class<?>, TypeConverter<?, ?>>>();
		final Map<Class<?>, TypeConverter<?, ?>> bigdecimalTargetConverter = new HashMap<Class<?>, TypeConverter<?, ?>>();
		bigdecimalTargetConverter.put(Long.class,
				new BigDecimalToLongConverter());
		bigdecimalTargetConverter.put(Integer.class,
				new BigDecimalToIntegerConverter());
		bigdecimalTargetConverter.put(long.class,
				new BigDecimalToLongConverter());
		bigdecimalTargetConverter.put(int.class,
				new BigDecimalToIntegerConverter());
		bigdecimalTargetConverter.put(Enum.class,
				new BigDecimalToEnumConverter());
		knownConverterMap.put(BigDecimal.class, bigdecimalTargetConverter);

		return knownConverterMap;
	}

	private <T> T createBean(ResultSet rs, Class<T> type) throws SQLException {
		try {
			// partial population, no eager/lazy support. Just for test
			final T entity = type.newInstance();
			final ResultSetMetaData metaData = rs.getMetaData();
			for (int col = 0; col < metaData.getColumnCount(); col++) {
				final int column = 1 + col;
				Object val = rs.getObject(column);
				String columnName = metaData.getColumnLabel(column);
				if (val == null) {
					continue;
				}
				for (Field field : entity.getClass().getDeclaredFields()) {
					final Class<?> fieldType = field.getType();
					Column aColumn = field.getAnnotation(Column.class);
					JoinColumn joinColumn = field
							.getAnnotation(JoinColumn.class);
					EmbeddedId embeddedId = field
							.getAnnotation(EmbeddedId.class);
					if (aColumn != null) {
						processColumn(entity, val, columnName, field,
								fieldType, aColumn);
					} else if (joinColumn != null
							&& joinColumn.name().equalsIgnoreCase(columnName)) {
						final Object newInstance = field.getType()
								.newInstance();
						FieldUtils.writeDeclaredField(entity, field.getName(),
								newInstance, true);
						processJoinObjectId(newInstance, val);
					} else if (embeddedId != null) {
						final Object current = FieldUtils.readDeclaredField(
								entity, field.getName(), true);
						if (current == null) {
							final Object newInstance = field.getType()
									.newInstance();
							FieldUtils.writeDeclaredField(entity,
									field.getName(), newInstance, true);
						}
						processEmbeddedObject(current, val, columnName);
					} else if (!Modifier.isStatic(field.getModifiers())) {
						// lazy load not supported (yet)
						LOGGER.debug(String.format("field '%s' not processed",
								field.getName()));
					}

				}
			}
			return entity;
		} catch (SQLException sql) {
			throw sql;
		} catch (Exception x) {
			throw new RuntimeException(x);
		}
	}

	private void processJoinObjectId(Object joinObject, Object val)
			throws IllegalAccessException {
		final Field idField = FieldUtils.getField(joinObject.getClass(), "id",
				true);
		final Class<?> fieldType = idField.getType();
		if (fieldType.isAssignableFrom(val.getClass())) {
			FieldUtils.writeDeclaredField(joinObject, idField.getName(), val,
					true);
		} else {
			Object matchingType = convert(val, fieldType);
			FieldUtils.writeDeclaredField(joinObject, idField.getName(),
					matchingType, true);
		}
	}

	private <T> void processColumn(final T entity, Object val,
			String columnName, Field field, final Class<?> fieldType,
			Column aColumn) throws IllegalAccessException {
		if (columnName.equalsIgnoreCase(aColumn.name())) {
			if (fieldType.isAssignableFrom(val.getClass())) {
				FieldUtils.writeDeclaredField(entity, field.getName(), val,
						true);
			} else {
				Object matchingType = convert(val, fieldType);
				FieldUtils.writeDeclaredField(entity, field.getName(),
						matchingType, true);
			}
		}
	}

	private void processEmbeddedObject(Object current, Object val,
			String columnName) throws IllegalAccessException {
		for (Field field : current.getClass().getDeclaredFields()) {
			final Class<?> fieldType = field.getType();
			Column aColumn = field.getAnnotation(Column.class);
			if (aColumn != null) {
				processColumn(current, val, columnName, field, fieldType,
						aColumn);
			}
		}
	}

	@SuppressWarnings("unchecked")
	private Object convert(Object val, Class<?> fieldType) {
		@SuppressWarnings("rawtypes")
		final TypeConverter converter = findConverter(val.getClass(), fieldType);
		if (converter == null) {
			LOGGER.warn(String.format("no converter for types (%s -> %s) ", val
					.getClass().getName(), fieldType.getName()));
			return null;
		}
		return converter.convertedFrom(val, fieldType);
	}

	private TypeConverter<?, ?> findConverter(Class<?> fromType, Class<?> toType) {
		if (converterMap.containsKey(fromType)) {
			final Map<Class<?>, TypeConverter<?, ?>> map = converterMap
					.get(fromType);
			TypeConverter<?, ?> toTypeConverter = map.get(toType);
			if (toTypeConverter == null) {
				Class<?> superclass = toType.getSuperclass();
				while (toTypeConverter == null
						&& !Object.class.equals(superclass)) {
					toTypeConverter = map.get(superclass);
					superclass = superclass.getSuperclass();
				}
			}
			return toTypeConverter;
		}
		return null;
	}

}
