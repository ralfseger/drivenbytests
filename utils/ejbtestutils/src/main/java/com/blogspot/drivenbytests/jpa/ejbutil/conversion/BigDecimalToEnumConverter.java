package com.blogspot.drivenbytests.jpa.ejbutil.conversion;

import java.math.BigDecimal;

@SuppressWarnings("rawtypes")
public class BigDecimalToEnumConverter implements
		TypeConverter<BigDecimal, Enum> {

	@Override
	public Enum convertedFrom(BigDecimal from, Class<?> toType) {
		return (Enum) toType.getEnumConstants()[from.intValue()];
	}

}