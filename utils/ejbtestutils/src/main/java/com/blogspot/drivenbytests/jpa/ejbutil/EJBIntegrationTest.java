package com.blogspot.drivenbytests.jpa.ejbutil;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Properties;

import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.commons.dbutils.BasicRowProcessor;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.ResultSetHandler;
import org.apache.openejb.jee.JAXBContextFactory;
import org.apache.openejb.jee.jpa.unit.Persistence;
import org.apache.openejb.jee.jpa.unit.PersistenceUnit;
import org.eclipse.persistence.config.EntityManagerProperties;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.blogspot.drivenbytests.jpa.util.DefaultTestData;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.googlecode.flyway.core.Flyway;

public abstract class EJBIntegrationTest<DATA extends DefaultTestData, EJBTYPE> {

	private static final String LOCAL_DATASOURCE_NAME = "localDataSource";
	private static final String PERSISTENCE_UNIT_NAME = "contractLocal";

	private static BasicDataSource datasource;
	private static EJBContainer ejbContainer;

	protected DATA testData;

	private Context context;

	protected abstract EJBTYPE getEjb();

	protected abstract DATA initTestData();

	protected abstract List<Class<?>> entitiesInLoeschReihenfolge();

	@BeforeClass
	public static void setUpEnvironment() throws Exception {
		datasource = initDatasource();	
		Flyway flyway = new Flyway();
		flyway.setDataSource(datasource);
		final String dbId = datasource.getUrl().split(":")[1];
		flyway.setLocations("classpath:schema/"+dbId+"/");
		flyway.migrate();
		
		final Properties properties = new Properties();
		properties.put(LOCAL_DATASOURCE_NAME, "new://Resource?type=DataSource");
		properties.put(LOCAL_DATASOURCE_NAME + ".JdbcDriver", datasource.getDriverClassName());
		properties.put(LOCAL_DATASOURCE_NAME + ".JdbcUrl", datasource.getUrl());
		properties.put(LOCAL_DATASOURCE_NAME + ".JtaManaged", "true");
		properties.put(LOCAL_DATASOURCE_NAME + ".UserName", datasource.getUsername());
		properties.put(LOCAL_DATASOURCE_NAME + ".Password", datasource.getPassword());
		properties.put("eclipselink.logging.level", "OFF");
		ejbContainer = EJBContainer.createEJBContainer(properties);
	}
	
	@Before
	public void setUp() throws Exception {
		testData = initTestData();
		context = ejbContainer.getContext();
		context.bind("inject", this);
	}

	@Test
	public void ejbConfigurationIsValid() {
		assertThat(getEjb(), notNullValue());
	}

	@After
	public void cleanUp() throws Exception {
		clearDatabase();
		context.close();
	}
	
	@AfterClass
	public static void cleanUpEnvironment() {
		ejbContainer.close();
	}

	private void clearDatabase() throws SQLException {
		final QueryRunner run = new QueryRunner(datasource);
		for (Class<?> entityKlasse : entitiesInLoeschReihenfolge()) {
			run.update("DELETE FROM " + entityKlasse.getSimpleName().toLowerCase());
		}
	}

	protected int ermittleAusDb(Class<?> entityClass) throws SQLException {
		final QueryRunner run = new QueryRunner(datasource);
		return run.query("SELECT count(*) FROM " + entityClass.getSimpleName().toLowerCase(), new ResultSetHandler<Integer>() {

			@Override
			public Integer handle(ResultSet rs) throws SQLException {
				if (rs.next()) {
					return ((Number) rs.getObject(1)).intValue();
				}
				return -1;
			}
		});
	}

	protected <T> T leseAusDb(final Class<T> entityClass, final long id) throws SQLException {
		final String sqlQuery = String.format("SELECT * FROM %s WHERE id = %d", entityClass.getSimpleName().toLowerCase(), id);
		return doInConnection(new WithResultSet<T>() {

			@Override
			public T doResultSet(ResultSet resultSet) throws SQLException {
				List<T> beanList = new BasicRowProcessor(new EntityDbBeanProcessor()).toBeanList(resultSet, entityClass);
				if (beanList == null || beanList.isEmpty()) {
					return null;
				}
				if (beanList.size() > 1) {
					throw new UnsupportedOperationException("ambiguos id");
				}
				return beanList.get(0);
			}
		}, sqlQuery);
	}

	protected <T> List<T> leseAusDb(final Class<T> entityClass) throws SQLException {
		final String sqlQuery = String.format("SELECT * FROM %s", entityClass.getSimpleName().toLowerCase());
		return doInConnection(new WithResultSet<List<T>>() {

			@Override
			public List<T> doResultSet(ResultSet resultSet) throws SQLException {
				return new BasicRowProcessor(new EntityDbBeanProcessor()).toBeanList(resultSet, entityClass);
			}
		}, sqlQuery);
	}

	private static BasicDataSource initDatasource() throws JAXBException {
		final JAXBContext jc = JAXBContextFactory.newInstance(Persistence.class);
		final Unmarshaller unmarshaller = jc.createUnmarshaller();
		final InputStream resource = EJBIntegrationTest.class.getResourceAsStream("/META-INF/persistence.xml");
		final Persistence persistence = (Persistence) unmarshaller.unmarshal(resource);
		final PersistenceUnit persistenceUnit = Iterables.filter(persistence.getPersistenceUnit(), new Predicate<PersistenceUnit>() {
			@Override
			public boolean apply(PersistenceUnit input) {
				return PERSISTENCE_UNIT_NAME.equals(input.getName());
			}
		}).iterator().next();
		final BasicDataSource datasource = new BasicDataSource();
		datasource.setDriverClassName(persistenceUnit.getProperty(EntityManagerProperties.JDBC_DRIVER));
		datasource.setUrl(persistenceUnit.getProperty(EntityManagerProperties.JDBC_URL));
		datasource.setUsername(persistenceUnit.getProperty(EntityManagerProperties.JDBC_USER));
		datasource.setPassword(persistenceUnit.getProperty(EntityManagerProperties.JDBC_PASSWORD));
		return datasource;
	}

	private interface WithResultSet<T> {

		T doResultSet(final ResultSet rs) throws SQLException;
	}

	private <T> T doInConnection(WithResultSet<T> processor, final String sqlQuery) throws SQLException {
		final Connection connection = datasource.getConnection();
		final Statement statement = connection.createStatement();
		final ResultSet resultSet = statement.executeQuery(sqlQuery);
		try {
			return processor.doResultSet(resultSet);
		} finally {
			if (resultSet != null) {
				DbUtils.closeQuietly(resultSet);
			}
			DbUtils.closeQuietly(statement);
			DbUtils.closeQuietly(connection);
		}
	}
}
