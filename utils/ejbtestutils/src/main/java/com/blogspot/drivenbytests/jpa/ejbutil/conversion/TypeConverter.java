package com.blogspot.drivenbytests.jpa.ejbutil.conversion;

public interface TypeConverter<FROM, TO> {

	TO convertedFrom(FROM from, Class<?> toType);

}
