package com.blogspot.drivenbytests.jpa.ejbutil;

import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.enterprise.inject.Produces;

@EJB(beanInterface = com.google.common.util.concurrent.Service.class, beanName = "CDIDeploymentProviderBean", name = "theCDIDeploymentProviderImpl")
@Stateless(name = "CDIDeploymentProviderEJB")
public class CDIDeploymentProvider {

	@Produces
	public Set<com.google.common.util.concurrent.Service> serviceManagerDependency() {
		throw new AssertionError();
	}
}
