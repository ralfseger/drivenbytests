package com.blogspot.drivenbytests;

import java.io.IOException;

import org.concordion.integration.junit4.ConcordionRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;

import com.blogspot.drivenbytests.pages.DepartmentTestPage;
import com.blogspot.drivenbytests.util.TestConfiguration;


@RunWith(ConcordionRunner.class)
public class DepartmentTest {

	private DepartmentTestPage page;
	private TestConfiguration configuration;

	public DepartmentTest() {
	}

	@Before
	public void setUp() throws IOException {
		configuration = TestConfiguration.getInstance();
	}

	@After
	public void tearDown() {
		configuration.cleanUp();
		
	}

	public String setDepartmentNameHasNumberOfEmloyees(final String name) {
		page = openTestPage();
		page.setDepartmentName(name);
		return page.countEmployees();
	}

	private DepartmentTestPage openTestPage() {
		if (page == null) {
			page = configuration.loadPage(DepartmentTestPage.class, "department.jsf");
		}
		return page;
	}

}
