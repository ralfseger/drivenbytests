package com.blogspot.drivenbytests.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.blogspot.drivenbytests.util.web.ConfiguredTestPage;


public class DepartmentTestPage extends ConfiguredTestPage{

	private static final String DEPT_TABLE_XPATH_EXPR = "//div[contains(./@id, 'departmentsDetailTable')]//table[@_rowcount]";

	@FindBy(xpath = "//ul[contains(./@id, 'departmentNameSelectionList')]")
	private WebElement departmentSelector;

	@FindBy(xpath = DEPT_TABLE_XPATH_EXPR)
	private WebElement dataTable;

	public void setDepartmentName(String name) {
		driver.setListByName(departmentSelector, name, DEPT_TABLE_XPATH_EXPR+"//td");
	}

	public String countEmployees() {
		return dataTable.getAttribute("_rowcount");
	}

}
